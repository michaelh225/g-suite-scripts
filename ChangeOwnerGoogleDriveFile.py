#!/usr/bin/env python3

#Supply in params the Drive folder ID of root shared folder and email address of account that will own files.

import json
from sys import argv
from httplib2 import Http
from oauth2client.service_account import ServiceAccountCredentials
from apiclient.discovery import build
from apiclient import errors
import time

script, paramFileId, paramCurrentUsername, paramTargetUsername = argv

scopes = ['https://www.googleapis.com/auth/drive']
credentials = ServiceAccountCredentials.from_json_keyfile_name(
              'service-account.json', scopes)
delegated_credentials = credentials.create_delegated(paramTargetUsername)
service = build('drive', 'v3', credentials=delegated_credentials)

def main():
    #One run through of iterate_files to handle anything in root folder.  Necessary because everything past here keys on parent folder id.
    create_permission(paramFileId, paramCurrentUsername, paramTargetUsername)

def create_permission(fileId, currentFileOwner, targetUsername):
  try:
        delegated_credentials = credentials.create_delegated(currentFileOwner)
        delegated_credentials_service = build('drive', 'v3', credentials=delegated_credentials)
        new_permission = {
        'type': 'user',
        'role': 'owner',
        'emailAddress': targetUsername
        }
        delegated_credentials_service.permissions().create(fileId=fileId, sendNotificationEmail='false', transferOwnership='true', body=new_permission).execute()
        print (('Changed permissions of file id') + fileId + (' to ') + targetUsername)
        return
  except errors.HttpError as error:
        print ('An error has occurred: %s' % error)
if __name__ == "__main__":
   main()
