#!/usr/bin/env python3

#Supply in params the Drive folder ID of root shared folder and email address of account that will own files.

import json
from sys import argv
from httplib2 import Http
from oauth2client.service_account import ServiceAccountCredentials
from apiclient.discovery import build
from apiclient import errors
import time

script, paramStartingFolderId, paramTargetUsername = argv

#Get allowed domains from file
domains = []
allowedDomainsFile = open("AllowedDomains.txt","r")
for values in allowedDomainsFile:
    domains.append(values.strip('\n'))
allowedDomainsFile.close()

scopes = ['https://www.googleapis.com/auth/drive']
credentials = ServiceAccountCredentials.from_json_keyfile_name(
              'service-account.json', scopes)
delegated_credentials = credentials.create_delegated(paramTargetUsername)
service = build('drive', 'v3', credentials=delegated_credentials)

def main():
    #One run through of iterate_files to handle anything in root folder.  Necessary because everything past here keys on parent folder id.
    iterate_files(paramStartingFolderId, paramTargetUsername)
    #Start our loops through folders.
    iterate_folders(paramStartingFolderId, paramTargetUsername)

#Loop through folders that have startingFolderId as a parent.  This is a recursive function.
def iterate_folders(startingFolderId, targetUsername):
    page_token = None
    #iterate through folders
    while True:
          time.sleep(.1)
          response = service.files().list(q="mimeType='application/vnd.google-apps.folder' and'"+ startingFolderId +"' in parents",
                                          spaces='drive',
                                          fields='nextPageToken, files(id, name, parents, owners)',
                                          pageToken=page_token).execute()
          for file in response.get('files', []):
            #Call function iterate_files with id of each folder that is found.
            fileId = file.get('id')
            iterate_files(fileId, targetUsername)
            #Recurse this same function with any folder id found.
            iterate_folders(fileId, targetUsername)
          page_token = response.get('nextPageToken', None)
          if page_token is None:
            break;

#Get a list of files NOT owned by target username.  This makes our target list much smaller.
def iterate_files(startingFolderId, targetUsername):
    page_token = None
    #iterate through files in folder
    while True:
          response = service.files().list(q="'" + startingFolderId +"' in parents and not '" + targetUsername + "' in owners",
                                          spaces='drive',
                                          fields='nextPageToken, files(id, name, owners, mimeType)',
                                          pageToken=page_token).execute()
          for file in response.get('files', []):
            for owners in file.get('owners'):
                fileOwner = owners.get('emailAddress')
            fileId = (file.get('id'))
            print (('File ID is ')+ fileId)
            print (('Current owner is ') + fileOwner)
            for domain in domains:
                if domain in fileOwner:
                    create_permission(fileId, fileOwner, targetUsername)
          page_token = response.get('nextPageToken', None)
          if page_token is None:
            break;

def create_permission(fileId, currentFileOwner, targetUsername):
  try:
        delegated_credentials = credentials.create_delegated(currentFileOwner)
        delegated_credentials_service = build('drive', 'v3', credentials=delegated_credentials)
        new_permission = {
        'type': 'user',
        'role': 'owner',
        'emailAddress': targetUsername
        }
        delegated_credentials_service.permissions().create(fileId=fileId, sendNotificationEmail='false', transferOwnership='true', body=new_permission).execute()
        print (('Changed permissions of file id ') + fileId + (' to ') + targetUsername)
        return
  except errors.HttpError as error:
        print ('An error has occurred: %s' % error)
if __name__ == "__main__":
   main()
